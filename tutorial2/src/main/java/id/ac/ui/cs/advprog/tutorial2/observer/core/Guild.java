package id.ac.ui.cs.advprog.tutorial2.observer.core;

import java.util.ArrayList;
import java.util.List;

public class Guild {
        private List<Adventurer> adventurers = new ArrayList<>();
        private Quest quest;

        public void add(Adventurer adventurer) {
                adventurers.add(adventurer);
        }

        public void addQuest(Quest quest) {
                this.quest = quest;
                broadcast();

        }

        public void broadcastQuest(Quest quest) {
                this.quest = quest;
                broadcast();
        }

        private void broadcast() {
                for (Adventurer adventurer: adventurers) {
                        adventurer.update();
                }
        }

        public String getQuestType () {return quest.getType();}

        public Quest getQuest() {return quest;}

        public List<Adventurer> getAdventurers() {
                return adventurers;
        }
}
