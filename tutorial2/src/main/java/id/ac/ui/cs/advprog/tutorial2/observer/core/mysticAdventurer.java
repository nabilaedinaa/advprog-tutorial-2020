package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class mysticAdventurer extends Adventurer {
    public mysticAdventurer(String name, Guild guild) {
        super(name, guild);
    }

    @Override
    public void update() {
        if (this.guild.getQuestType().equalsIgnoreCase("D") || this.guild.getQuestType().equalsIgnoreCase("E")) {
            this.getQuests().add(this.guild.getQuest());
        }

    }
}
