package id.ac.ui.cs.advprog.tutorial2.observer.core;

import java.util.ArrayList;
import java.util.List;

public class Adventurer {
        protected Guild guild;
        protected String name;
        private List<Quest> quests = new ArrayList<>();

        public Adventurer(String name, Guild guild) {
                this.name = name;
                this.guild = guild;
                this.guild.add(this);

        }

        public void update() {
                if (this.name.equalsIgnoreCase("Agile")) {
                        if (this.guild.getQuestType().equalsIgnoreCase("D") || this.guild.getQuestType().equalsIgnoreCase("R")) {
                                this.getQuests().add(this.guild.getQuest());
                        }
                }
                else if (this.name.equalsIgnoreCase("Mystic")) {
                        if (this.guild.getQuestType().equalsIgnoreCase("D") || this.guild.getQuestType().equalsIgnoreCase("E")) {
                                this.getQuests().add(this.guild.getQuest());
                        }
                }
                else if (this.name.equalsIgnoreCase("Knight")) {
                        this.getQuests().add(this.guild.getQuest());
                }
        };

        public String getName() {
                return this.name;
        }

        public List<Quest> getQuests() {
                return this.quests;
        }
}
