package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class knightAdventurer extends Adventurer {
    public knightAdventurer(String name, Guild guild) {
        super(name, guild);
    }

    @Override
    public void update() {
        this.getQuests().add(this.guild.getQuest());
    }
}
