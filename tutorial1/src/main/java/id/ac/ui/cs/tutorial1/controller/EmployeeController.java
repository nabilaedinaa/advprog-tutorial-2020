package id.ac.ui.cs.tutorial1.controller;

import id.ac.ui.cs.tutorial1.service.GuildEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    GuildEmployeeService guildEmployeeService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    private String home(Model model) {
        model.addAttribute("employee", guildEmployeeService.findAll());
        return "home";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    private String addEmployeePage() {
        return "add-employee";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    private ModelAndView addEmployeeToSystem(HttpServletRequest request) {
        //System.out.println(request.getParameter("type"));
        guildEmployeeService.addEmployee(request.getParameter("employeeName"), request.getParameter("familyName"),
                request.getParameter("birthDate"), request.getParameter("type"));
        return new ModelAndView( "redirect:/employee/");
    }



}
